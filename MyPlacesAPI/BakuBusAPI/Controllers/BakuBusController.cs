﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BakuBusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BakuBusController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<string>> Get()
        {
            HttpClient httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("https://www.bakubus.az/az/ajax/apiNew1");
            return json;
        }
    }
}