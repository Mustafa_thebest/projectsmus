﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyPlacesAPI.Models;

namespace MyPlacesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyPlacesController : ControllerBase
    {
        private readonly MyPlacesAppDbContext context;

        public MyPlacesController(MyPlacesAppDbContext context)
        {
            this.context = context;
            //if (!context.MyPlaces.Any())
            //{
            //    context.MyPlaces.Add(new MyPlace
            //    {
            //        Name = "Academy",
            //        Decription = "Step IT",
            //        Id = 0,
            //        IsFavorite = true,
            //        Latitude = 49,
            //        Longitude = 49
            //    });

            //    context.MyPlaces.Add(new MyPlace
            //    {
            //        Name = "Otel",
            //        Decription = "Otelchik",
            //        Id = 0,
            //        IsFavorite = false,
            //        Latitude = 44,
            //        Longitude = 43
            //    });
            //    context.SaveChanges();
            //}
        }

        //GET /api/myplaces
        [HttpGet]
        public ActionResult<IEnumerable<MyPlace>> GetPlaces()
        {           
            return context.MyPlaces;
        }

        //GET /api/myplaces/id
        [HttpGet("{id}")]
        public ActionResult<MyPlace> GetPlace(int id)
        {
            return context.MyPlaces.FirstOrDefault(x => x.Id == id);
        }

        //POST /api/myplaces
        [HttpPost]
        public ActionResult AddPlaces(MyPlace myPlace)
        {
            if(ModelState.IsValid)
            {
                context.MyPlaces.Add(myPlace);
                context.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }

        //DELETE /api/myplaces/id
        [HttpDelete("{id}")]
        public ActionResult<MyPlace> DeletePlace(int id)
        {
            var place = context.MyPlaces.FirstOrDefault(x => x.Id == id);
            if(place == null) {
                return NotFound();
            }
            context.MyPlaces.Remove(place);
            context.SaveChanges();
            return Ok();
        }


    }
}