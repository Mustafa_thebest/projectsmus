﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyPlacesAPI.Models
{
    public class MyPlacesAppDbContext : DbContext
    {
        public MyPlacesAppDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<MyPlace> MyPlaces { get; set; }
    }
}
