﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyPlacesAPI.Models
{
    public class MyPlace
    {
        public int Id { get; set; }

        [Required]
        public string Name  { get; set; }


        public string Decription { get; set; }

        [Required]
        public float Latitude { get; set; }

        [Required]
        public float Longitude { get; set; }


        public bool IsFavorite { get; set; }
    }
}
