export class MyPlaces {
    id?: number;
    name: string;
    desc?: string;
    latitude: number;
    longitude: number;
    isFavorite?: boolean;
}
