import { Pipe, PipeTransform } from '@angular/core';
import { MyPlaces } from '../models/my-places';

@Pipe({
  name: 'favoritesFilter'
})
export class FavoritesFilterPipe implements PipeTransform {

  transform(value: Array<MyPlaces>, condition?: boolean): Array<MyPlaces> {
    return value.filter(x => x.isFavorite == condition);
  }

}
