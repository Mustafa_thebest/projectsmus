import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'busNumberFilter'
})
export class BusNumberFilterPipe implements PipeTransform {

  transform(buses: Array<any>, busNumber?: string): any {
    return buses.filter(x => x['@attributes'].DISPLAY_ROUTE_CODE == busNumber);
  }

}
 