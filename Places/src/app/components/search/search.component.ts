import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  CurrentLat: number;
  CurrentLong: number;
  places: any = [];

  constructor(private httpClient: HttpClient) {
    this.GetCurrentLocation();
  }


  async ngOnInit() {
    let response = await this.httpClient.get<any>(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${this.CurrentLat},${this.CurrentLong}&radius=500&key=AIzaSyAJ0f9T9JfPIaCBfaByN1AQ1dXUe7K4KtI`).toPromise();
    this.places = response.results;
    console.log(this.places);
  }
  
  GetCurrentLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.CurrentLat = position.coords.latitude;
      this.CurrentLong = position.coords.longitude;
    });
  }

}
