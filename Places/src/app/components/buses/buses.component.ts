import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-buses',
  templateUrl: './buses.component.html',
  styleUrls: ['./buses.component.scss']
})
export class BusesComponent implements OnInit {

  buses: any = [];
  CurrentLat: number;
  CurrentLong: number;
  busNumber: string = "7A";

  constructor(private httpClient: HttpClient) {
    this.GetCurrentLocation();
  }


  async ngOnInit() {

    let self = this;
    setInterval(async function() {
      let response = await self.httpClient.get<any>('http://localhost:53541/api/bakubus').toPromise();
      self.buses = response.BUS;
      console.log(self.buses);
    }, 1000);
    // let response = await this.httpClient.get<any>('http://localhost:53541/api/bakubus').toPromise();
    // this.buses = response.BUS;
    // console.log(this.buses);
  }

  GetCurrentLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.CurrentLat = position.coords.latitude;
      this.CurrentLong = position.coords.longitude;
    });
  }

}
