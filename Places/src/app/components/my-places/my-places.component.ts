import { Component, OnInit } from '@angular/core';
import { MyPlaces } from 'src/app/models/my-places';
import { FavoritesFilterPipe } from 'src/app/pipes/favorites-filter.pipe';
import { stringify } from 'querystring';
import { CurrentLocation } from 'src/app/models/current-location';
import { CombineLatestOperator } from 'rxjs/internal/observable/combineLatest';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-my-places',
  templateUrl: './my-places.component.html',
  styleUrls: ['./my-places.component.scss']
})
export class MyPlacesComponent implements OnInit {
  newPlace: MyPlaces = { name: null, latitude: null, longitude: null };
  messageColor: string = '#000';
  infoMessage: string = null;
  CurrentLat: number;
  CurrentLong: number;
  myPlaces: Array<MyPlaces> = [];
  title = 'Places';
  // lat: number = 40.414852;
  // lng: number = 49.853336;
  IsShowFavorite: boolean = false;

  constructor(private httpClient: HttpClient) {
    

    this.GetCurrentLocation();
   }

  async onDelete(id: number) {
    try {
      await this.httpClient.delete(`http://localhost:49296/api/myplaces/${id}`).toPromise();
      console.log('Delete!'); 
      this.ngOnInit();
    } catch (error) {
      console.log(error);
    }
  }

  ClearMessage() {
    this.infoMessage = " ";
    console.log("ff");
  }
   
  async onSubmit() {
    try {
      await this.httpClient.post('http://localhost:49296/api/myplaces', this.newPlace).toPromise();   
      this.ngOnInit();
      this.messageColor = "green";
      this.infoMessage = "You have successfully added place!"
    } catch (error) {
      console.log(error);
      this.messageColor = "red";
      this.infoMessage = error.error.Title[0];
    }
  }

  async ngOnInit() {
    let result = await this.httpClient.get<Array<MyPlaces>>('http://localhost:49296/api/myplaces/').toPromise();
    this.myPlaces = result;
  }

  ChangeStatus(item: any) {
    if(item.isFavorite == true) {
      item.isFavorite = false;
      // this.httpClient.
      console.log(item.isFavorite);
    } else {
      item.isFavorite = true;
      console.log(item.isFavorite);
    } 
  }

  ShowFavorites() {
    this.IsShowFavorite = !this.IsShowFavorite;
  }

  GetCurrentLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.CurrentLat = position.coords.latitude;
      this.CurrentLong = position.coords.longitude;
    });
  }
}
