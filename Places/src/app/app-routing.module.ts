import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MyPlacesComponent } from './components/my-places/my-places.component';
import { SearchComponent } from './components/search/search.component';
import { BusesComponent } from './components/buses/buses.component';
import { ErrorComponent } from './components/error/error.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'my-places', component: MyPlacesComponent},
  {path: 'search', component: SearchComponent},
  {path: 'buses', component: BusesComponent},
  {path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
