import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MyPlacesComponent } from './components/my-places/my-places.component';
import { SearchComponent } from './components/search/search.component';
import { BusesComponent } from './components/buses/buses.component';
import { ErrorComponent } from './components/error/error.component';
import { FavoritesFilterPipe } from './pipes/favorites-filter.pipe';
import { BusNumberPipe } from './pipes/bus-number.pipe';
import { BusNumberFilterPipe } from './pipes/bus-number-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MyPlacesComponent,
    SearchComponent,
    BusesComponent,
    ErrorComponent,
    FavoritesFilterPipe,
    BusNumberPipe,
    BusNumberFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAJ0f9T9JfPIaCBfaByN1AQ1dXUe7K4KtI'
    }),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
